import React from "react";
import ChoosenItems from "../../components/ChoosenItems/ChoosenItems";

const Choosen = () => {

    return(
        <div className="container">
            <ChoosenItems />
        </div>
    )
}

export default Choosen;