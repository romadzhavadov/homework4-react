import React, { useEffect, memo } from 'react';
import { useDispatch} from 'react-redux'
import Header from './components/Header/Header';
import AppRoutes from './AppRoates';
import './App.scss';
import { fetchGoods } from './redux/actionCreators/goodsActionCreators';
import Modal from './components/Modal/Modal';

function App() {

const dispatch = useDispatch()

useEffect(() => {
  dispatch(fetchGoods())
}, [dispatch])

  return (
    <>
      <Header />
      <AppRoutes />
      <Modal />
    </>
  )
}



export default memo(App);
