import React from "react";
import styles from './Button.module.scss'
import classNames from 'classnames'
import PropTypes from "prop-types"

const Button = (props) => {

const { children, onClick } = props;

    return(
        <button onClick={onClick} className={classNames(styles.btn)}>{children}</button>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
}

Button.defaultProps = {
    onClick: () => {},
}

export default Button;