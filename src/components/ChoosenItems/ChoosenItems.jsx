import React from "react";
import ChoosenCard from "../ChoosenCard/ChoosenCard";
import { shallowEqual, useSelector } from "react-redux";

const ChoosenItems = () => {

    const items = useSelector(state => state.chosen.choosenArr, shallowEqual)
    
    return(
        <section>
            <div className="cardWrap">
            <h2>Choosen Laptops</h2>
                {items.length > 0 ? items.map(({name, price, url, id, isFavourite, color, count}) => <ChoosenCard key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} count={count} />) : <p className="noitems"> No items have been added </p> }
            </div>
        </section>
    )

}


export default ChoosenItems;