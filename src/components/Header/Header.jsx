import React, { memo } from "react";
import styles from "./Header.module.scss"
// import StarIcon from "../svgComponents/StarIcon";
import { ReactComponent as StarIcon} from "../../asset/star.svg"
import { ReactComponent as BasketIcon} from "../../asset/basket.svg"
import { NavLink} from 'react-router-dom'
import classNames from "classnames";
import { useSelector } from "react-redux";


const Header = () => {

    const favItems = useSelector(state => state.goods.goodsArr);
    const choosenItems = useSelector(state => state.chosen.choosenArr);

    let starAmount = 0;
    favItems.forEach(el => {
        if(el.isFavourite) {
            starAmount++
        }
    });

    const basketAmount = choosenItems.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.count;
    }, 0);

    return(

        <header className={styles.header}>
            <span>LAPTOP Store</span>
            <div className={styles.nav}>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/'>Home</NavLink>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/favourite'>Favourite</NavLink>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/choosen'>Choosen</NavLink>
            </div>
            <div className={styles.wrap}>
                <div > 
                    <StarIcon/>
                    <span>{starAmount}</span>
                </div>
                <div > 
                    <BasketIcon />
                    <span>{basketAmount}</span>
                </div>
            </div>
        </header>
    )
}

export default memo(Header);