import { combineReducers } from 'redux';
import goodsReducer from './goodsRedecer';
import chosenReducer from './choosenReducer';
import modalReducer from './modalReducer';

const appReducer = combineReducers({
    goods: goodsReducer,
    chosen: chosenReducer,
    modal: modalReducer
})

export default appReducer;